//
//  Double+Ext.swift
//  CHUC K
//
//  Created by Константин on 06/03/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import Foundation

extension Double {
    
    func rounded(to x: Int) -> Double {
        let divisor = pow(10.0, Double(x))
        return (self * divisor).rounded() / divisor
    }
    
    var degreesToRadians: Double {
        get {
            return self * .pi / 180.0
        }
    }
    
    var radiansToDegrees: Double {
        get {
            return self * 180.0 / .pi
        }
    }
    
}
