//
//  OfficesDataSource.swift
//  CHUC_iOS
//
//  Created by Константин on 22/01/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import Foundation

protocol OfficesDataSource: class {
    
    func numberOfItems() -> Int
    func item(at index: Int) -> Room
    func textChanged(newText: String)
    
}
