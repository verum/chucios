//
//  MockDS.swift
//  CHUC_iOS
//
//  Created by Константин on 22/01/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import Foundation

class RealDS: OfficesDataSource {
    
    private var searchString: String = ""
    
    lazy private var items: [Room] = {
        return MainService.shared.mainLocation?.floors.reduce(into: [], { result, element in
            result += element.rooms
        }) ?? []
    }()
    
    var filteredItems: [Room] {
        get {
            return items.filter({searchString.isEmpty || [$0.title?.text, $0.number].compactMap({$0}).joined().lowercased().contains(searchString.lowercased())})
        }
    }
    
    func numberOfItems() -> Int {
        return filteredItems.count
    }
    
    func item(at index: Int) -> Room {
        return filteredItems[index]
    }
    
    func textChanged(newText: String) {
        searchString =  newText
    }
}
