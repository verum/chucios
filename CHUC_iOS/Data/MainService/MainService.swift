//
//  MainService.swift
//  CHUC K
//
//  Created by Константин on 26/02/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import Foundation
import Alamofire
import IndoorAtlas
import SDWebImage


class MainService {
    
    static let shared = MainService()
    
    private(set) lazy var mainLocation: Location? = {
        guard let path = Bundle.main.path(forResource: "main", ofType: "json") else { return nil }
        guard let jsonData = try? Data(contentsOf: URL(fileURLWithPath: path)) else { return nil }
        
        let location = try? JSONDecoder().decode(Location.self, from: jsonData)
        return location
    }()
    
    private(set) lazy var iaManager: IALocationManager = {
        let m = IALocationManager.sharedInstance()
        m.desiredAccuracy = .iaLocationAccuracyBest
        return m
    }()
    
    static func floor(for room: Room) -> Floor? {
        return shared.mainLocation?.floors.first(where: { $0.rooms.contains(room) })
    }
    
    static func floor(for level: Int?) -> Floor? {
        if level == nil {
            return shared.mainLocation?.floors.min()
        }
        return shared.mainLocation?.floors.first(where: { $0.level == level })
    }
    
    
    private(set) var images: [Int: (Floor?, UIImage?)] = [:]
    private var plansLoaded = false
    
    func startLoading() {
        guard let location = mainLocation else {
            assertionFailure("can't create main location from json")
            return
        }
        
        var plansRequested = 0
        
        func increment() {
            
            plansRequested += 1
            
            if plansRequested == location.floors.count {
                self.plansLoaded = true
                self.plansSubscriptions.forEach({ $0?() })
                return;
            }
        }
        
        for floor in location.floors {
            var levelLabel = ""
            if (floor.level > -1) {
                levelLabel = "\(floor.level)"
            } else {
                levelLabel = "m\(abs(floor.level))"
            }
            let imageName = "level_\(levelLabel)"
            self.images[floor.level] = (floor, UIImage(named: imageName))
        }
        self.plansLoaded = true
        self.plansSubscriptions.forEach({ $0?() })
        
        
//        for floor in location.floors {
//
//            let url = URL(string: floor.url)
//            SDWebImageManager.shared().loadImage(with: url, options: [.retryFailed, .continueInBackground], progress: nil, completed: { (img, data, imgLoadingError, _, _, _) in
//
//                if let imgLoadingError =  imgLoadingError {
//                    print("imgLoadingError = \(imgLoadingError)")
//                }
//
//                if (img?.size.width ?? 0) > 1080 {
//                    let scaledImage = img?.scale(toWidth: 1080)
//                    SDWebImageManager.shared().saveImage(toCache: scaledImage, for: url)
//
//                    self.images[floor.level] = (floor, scaledImage)
//                } else {
//                    self.images[floor.level] = (floor, img)
//                }
//
//                increment()
//            })
//
//
//        }
        
    }
    
    
    private var plansSubscriptions: [(()->Void)?] = []
    
    func subscribeForPlans(completion: @escaping ()->Void) {
        if plansLoaded {
            completion()
        } else {
            plansSubscriptions.append(completion)
        }
    }
    
    
    
    
    
}
