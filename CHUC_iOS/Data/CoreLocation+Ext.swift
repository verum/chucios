//
//  CLLocationCoordinate2D+Ext.swift
//  CHUC K
//
//  Created by Константин on 06/03/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import Foundation
import CoreLocation

extension CLLocation {
    
    func bearing(to other: CLLocation) -> CLLocationDegrees  {
        
        let lat1 = self.coordinate.latitude.degreesToRadians
        let lon1 = self.coordinate.longitude.degreesToRadians
        
        let lat2 = other.coordinate.latitude.degreesToRadians
        let lon2 = other.coordinate.longitude.degreesToRadians
        
        let dLon = lon2 - lon1
        
        let y = sin(dLon) * cos(lat2)
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
        let radiansBearing = atan2(y, x)
        
        return radiansBearing.radiansToDegrees
    }
    
}


extension CLLocationCoordinate2D  {
    
    func bearing(to other: CLLocationCoordinate2D) -> CLLocationDegrees  {
        let currentloc = CLLocation(latitude: self.latitude, longitude: self.longitude)
        let otherLoc = CLLocation(latitude: other.latitude, longitude: other.longitude)
        return currentloc.bearing(to: otherLoc)
    }
    
    func intersectionWithLine(_ start: CLLocationCoordinate2D, _ end: CLLocationCoordinate2D) -> CLLocationCoordinate2D {
        let A = self.latitude - start.latitude;
        let B = self.longitude - start.longitude;
        
        let C = end.latitude - start.latitude;
        let D = end.longitude - start.longitude;
        
        let dot = A * C + B * D;
        let len_sq = C * C + D * D;
        let param = dot / len_sq;
        
        var xx: Double!
        var yy: Double!
        
        if (param < 0 || (start.latitude == end.latitude && start.longitude == end.longitude)) {
            xx = start.latitude;
            yy = start.longitude;
        }
        else if (param > 1) {
            xx = end.latitude;
            yy = end.longitude;
        }
        else {
            xx = start.latitude + param * C;
            yy = start.longitude + param * D;
        }
        
        return CLLocationCoordinate2D(latitude: xx, longitude: yy)
        
    }
    
    
    func bearingDelta(to coordinate: CLLocationCoordinate2D, with bearing: CLLocationDegrees) -> CLLocationDegrees {
        return self.bearing(to: coordinate) - bearing
    }
    
    
    func distance(to coordinate: CLLocationCoordinate2D) -> CLLocationDistance {
        return CLLocation(latitude: self.latitude, longitude: self.longitude).distance(from: CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude))
    }
    
    func rotated(by angle: Double, around center: CLLocationCoordinate2D) -> CLLocationCoordinate2D {
        let tempX = self.latitude - center.latitude;
        let tempY = self.longitude - center.longitude;
        let theta =  angle  * .pi / 180
        
        let rotatedX = tempX*cos(theta) - tempY*sin(theta);
        let rotatedY = tempX*sin(theta) + tempY*cos(theta);
        
        return CLLocationCoordinate2D(latitude: rotatedX + center.latitude, longitude: rotatedY + center.longitude)
    }

}


func ==(lhs: CLLocationCoordinate2D!, rhs: CLLocationCoordinate2D!) -> Bool {
    guard lhs != nil && rhs != nil else { return lhs == nil && rhs == nil }
    return lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude
}
