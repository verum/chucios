//
//  Async.swift
//  CHUC K
//
//  Created by Константин on 06/03/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import Foundation

func async(after ms: Int = 0, _ block: @escaping ()->Void) {
    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + DispatchTimeInterval.milliseconds(ms), execute: block)
}
