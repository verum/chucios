//
//  CHLocationManagerDelegate.swift
//  CHUC K
//
//  Created by Константин on 11/03/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import Foundation
import IndoorAtlas

protocol CHLocationManagerDelegate: class {
    
    func chLocationManager(_ manager: CHLocationManager, didUpdate location: Coordinates)
    func chLocationManager(_ manager: CHLocationManager, didUpdateSDK sdkLocation: Coordinates)
    func chLocationManager(_ manager: CHLocationManager, didUpdate route: CustomRoute, nodeChanged: Bool)
    func chLocationManager(_ manager: CHLocationManager, didUpdate heading: CLLocationDirection)
    func chLocationManager(_ manager: CHLocationManager, didUpdate calibrationQuality: ia_calibration)

}
