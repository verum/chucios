//
//  CHLocationManager.swift
//  CHUC K
//
//  Created by Константин on 11/03/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import Foundation
import CoreLocation
import IndoorAtlas


class CHLocationManager: NSObject {
    
    static let shared = CHLocationManager()
    
    weak var delegate: CHLocationManagerDelegate?
    
    fileprivate var trackedLocations: [Coordinates] = []
    fileprivate var lockedFloor: Int? = nil
    
    lazy var clManager: CLLocationManager = {
        let m = CLLocationManager.init()
        m.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        return m
    }()
    
    
    var currentLocation: Coordinates? {
        get {
            return trackedLocations.last
        }
    }
    
    fileprivate var locationsBufferLastUpdate: Date!
    fileprivate var locationsBufferTimeOut: Double = 3
    fileprivate var locationsBufferSize = 3
    fileprivate var locationsBufferr: [Coordinates] = []
    
    private(set) lazy var graph: WayfindindGraph? =  {
        return WayfindindGraph.parse(json: "wayfinding")
    }()
    
    private(set) lazy var iaManager: IALocationManager = {
        let manager = MainService.shared.iaManager
        manager.delegate = self
        return manager
    }()
    
    override init() {
        super.init()
        
        if CLLocationManager.authorizationStatus() == .notDetermined {
            clManager.requestWhenInUseAuthorization()
        } else {
            clManager.startUpdatingLocation()
            clManager.startUpdatingHeading()
        }
    }
    
    
    func startUpdatingLocation() {
        iaManager.startUpdatingLocation()
    }
    
    func lock(floor: Int) {
        guard lockedFloor != floor else { return }
        graph?.reset()
        
        lockedFloor = floor
        if let last = trackedLocations.last {
            handle(new: last)
        }
    }
    
    func updateRouteIfNeeded(nodeChanged: Bool) {
        if let route = graph?.currentRoute {
            graph?.getRoute(to: route.end.clCoordinates, floor: route.end.floor, completion: { [weak self] (updatedRoute) in
                guard let updatedRoute = updatedRoute else { return }
                self?.delegate?.chLocationManager(self!, didUpdate: updatedRoute, nodeChanged: nodeChanged)
            })
        }
    }
    
}


extension CHLocationManager: IALocationManagerDelegate {
    
    func indoorLocationManager(_ manager: IALocationManager, calibrationQualityChanged quality: ia_calibration) {
        delegate?.chLocationManager(self, didUpdate: quality)
    }
    
    func indoorLocationManager(_ manager: IALocationManager, didUpdate newHeading: IAHeading) {
        delegate?.chLocationManager(self, didUpdate: newHeading.trueHeading)
    }
    
    func indoorLocationManager(_ manager: IALocationManager, didUpdateLocations locations: [Any]) {

        if let loc =  locations.first as? IALocation {
            if let location = loc.location {
                handle(new: Coordinates(coordinate: location.coordinate, floor: loc.floor?.level))
            }
        }
    }
    
    func indoorLocationManager(_ manager: IALocationManager, statusChanged status: IAStatus) {
        print(#function, status)
    }
    
    func indoorLocationManager(_ manager: IALocationManager, didReceiveExtraInfo extraInfo: [AnyHashable : Any]) {
        print(#function, extraInfo)
    }
    
    func indoorLocationManager(_ manager: IALocationManager, didEnter region: IARegion) {
        print(#function, region)
    }
    
    func indoorLocationManager(_ manager: IALocationManager, didExitRegion region: IARegion) {
        print(#function, region)
    }
}


extension CHLocationManager {
    
    func handle(new location: Coordinates) {
        
        locationsBufferr.append(location)
        
        if locationsBufferLastUpdate == nil ||  locationsBufferr.count == locationsBufferSize || Date().timeIntervalSince(locationsBufferLastUpdate) > locationsBufferTimeOut  {
            
            let middleLat = locationsBufferr.reduce(0, {
                return $0 + $1.clCoordinates.latitude
            })/Double(self.locationsBufferr.count)
            
            let middleLon = locationsBufferr.reduce(0, {
                return $0 + $1.clCoordinates.longitude
            })/Double(self.locationsBufferr.count)
            
            var middleCoordinates = CLLocationCoordinate2D(latitude: middleLat, longitude: middleLon)
            
            let bufferFloors = locationsBufferr.compactMap({$0.floor})
            var middleFloor: Int? = bufferFloors.count == 0 ? nil : bufferFloors.reduce(0, { res, element in res + element })/bufferFloors.count
            
            if lockedFloor == nil {
                lockedFloor = middleFloor
            } else {
                middleFloor = lockedFloor
            }
            
            
            var newLocation = Coordinates(coordinate: middleCoordinates, floor: middleFloor)
            
            delegate?.chLocationManager(self, didUpdateSDK: newLocation)
            
            let lastNode = graph?.routeHistory.last
            
          
            
            if let graph = graph {
                middleCoordinates = graph.onArrived(to: middleCoordinates, floor: middleFloor)
                newLocation = Coordinates(coordinate: middleCoordinates, floor: middleFloor)
            }

          
            
            trackedLocations.append(newLocation)
            delegate?.chLocationManager(self, didUpdate: newLocation)
            
            updateRouteIfNeeded(nodeChanged: lastNode != graph?.routeHistory.last)

        }
    }
    
}
