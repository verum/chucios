//
//  Room.swift
//  CHUC K
//
//  Created by Константин on 26/02/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import Foundation

class Room: Codable, Equatable {
    
    static func == (lhs: Room, rhs: Room) -> Bool {
        return lhs.number == rhs.number
    }
    
    var number = ""
    var title: RoomTitle?
    var location: RoomEntrance?
    var isExpended: Bool? = false
    

}


