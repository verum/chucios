//
//  Coordinates.swift
//  CHUC_iOS
//
//  Created by Константин on 22/01/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import Foundation
import CoreLocation

infix operator ~==

class Coordinates: NSObject, Codable {
    
    static let precision = 6
    
    private (set) var lat: Double!
    private (set) var lon: Double!
    private (set) var floor: Int?
    
    init(lat latitude: Double, lon longitude: Double, floor _floor: Int?) {
        lat = latitude
        lon = longitude
        floor = _floor
    }
    
    init(coordinate: CLLocationCoordinate2D, floor _floor: Int?) {
        lat = coordinate.latitude
        lon = coordinate.longitude
        floor = _floor
    }
    
    func rounded() -> Coordinates {
        lat = lat.rounded(to: Coordinates.precision)
        lon = lon.rounded(to: Coordinates.precision)
        return self
    }
    
    static func ~==(lhs: Coordinates, rhs: Coordinates) -> Bool {
        let lRounded = lhs.rounded()
        let rRounded = rhs.rounded()
        return lRounded.lat == rRounded.lat && lRounded.lon == rRounded.lon && lRounded.floor == rRounded.floor
    }
    
}

extension Coordinates {
    
    var clCoordinates: CLLocationCoordinate2D {
        get {
            return CLLocationCoordinate2D(latitude: lat, longitude: lon)
        }
    }
}
