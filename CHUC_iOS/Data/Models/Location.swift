//
//  Location.swift
//  CHUC K
//
//  Created by Константин on 26/02/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import Foundation

class Location: Codable {
    
    var name = ""
    var id = ""
    var floors: [Floor] = []
    var entrances: [BuildingEntrance] = []
    
}
