//
//  BuildingEntrance.swift
//  CHUC K
//
//  Created by Константин on 29/03/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import Foundation

class BuildingEntrance: Codable {
    
    var title: RoomTitle!
    private var location: Coordinates!
    private var level: Int!
    
    var coordinates: Coordinates! {
        get {
            return Coordinates(lat: location.lat, lon: location.lon, floor: level)
        }
    }
    
}
