//
//  WayfindingNode.swift
//  CHUC K
//
//  Created by Константин on 06/03/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import Foundation
import CoreLocation


class  WayfindingNode: Codable, Equatable, Hashable {
    
    var latitude: Double!
    var longitude: Double!
    var floor: Int!
    var index: Int!
    var cachedDistances: [Int: Double]!
    
    static func == (lhs: WayfindingNode, rhs: WayfindingNode) -> Bool {
        return lhs.index == rhs.index
    }
    
    lazy var coordinate: CLLocationCoordinate2D = {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }()
    
    lazy var location: CLLocation = {
        return CLLocation(latitude: latitude, longitude: longitude)
    }()
    
    func distance(to node: WayfindingNode) -> Double {
        if cachedDistances == nil {
            cachedDistances =  [:]
        }
        let coordinates = Coordinates(lat: node.latitude, lon: node.longitude, floor: floor)
        
        if let key = cachedDistances.keys.first(where: { $0 == node.index }), let value = cachedDistances[key] {
            return value
        } else {
            let floorDelta = abs(floor - node.floor)
            
            let distance: Double
            if (floorDelta == 0) {
                distance = location.distance(from: CLLocation(latitude: coordinates.lat, longitude: coordinates.lon))
            } else {
                distance = 1.0 / Double(floorDelta)
            }
            cachedDistances[node.index] = distance
            
            return cachedDistances[node.index]!
        }

    }
    
    func distance(to cords: CLLocationCoordinate2D) -> Double {
        return location.distance(from: CLLocation(latitude: cords.latitude, longitude: cords.longitude))
        
    }
    
    func bearingDelta(to node: WayfindingNode, with bearing: CLLocationDegrees) -> CLLocationDegrees {
        return self.location.bearing(to: node.location) - bearing
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(index)
    }
}
