//
//  CustomRoute.swift
//  CHUC K
//
//  Created by Константин on 06/03/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import Foundation
import CoreLocation


class CustomRoute: Codable {
    
    var start: Coordinates!
    var end: Coordinates!
    var edges: [WayfindingEdge] = []
    var encodedGMSPath: String?
    var gmsDistance: Double = 0
    var entrance: BuildingEntrance?
    
    
    func length(in graph: WayfindindGraph!) -> Double {
        return edges.reduce(0, { $0 + (graph?.length(for: $1) ?? 0) }) + self.gmsDistance
    }
    
    func locationChanged(to newLocation: Coordinates) {
        guard !(start == newLocation) else { return }
        
        start = newLocation
    }
}
