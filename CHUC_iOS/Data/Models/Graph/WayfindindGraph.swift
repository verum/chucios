//
//  WayfindindGraph.swift
//  CHUC_iOS
//
//  Created by Константин on 25/01/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import Foundation
import CoreLocation
import CoreGraphics
import Alamofire
import GoogleMaps

class GmLeg: Codable {
    var from: Coordinates!
    var to: Coordinates!
    
    init(from _from: Coordinates, to _to: Coordinates) {
        from = _from
        to = _to
    }
}

class WayfindindGraph: Codable {
    
    var nodes: [WayfindingNode] = []
    var edges: [WayfindingEdge] = []
    
    private enum CodingKeys: String, CodingKey {
        case nodes
        case edges
    }
    
    // begin dijkstra
    private var  settledNodes:Set<WayfindingNode> = Set()
    private var unSettledNodes: Set<WayfindingNode> = Set()
    private var  predecessors: [WayfindingNode : WayfindingNode] = [:]
    private var distance: [WayfindingNode : Double] = [:]
    // end dijkstra
    
    func nodes(for floor: Int) -> [WayfindingNode] {
        return nodes.filter({ $0.floor == floor })
    }
    
    func edges(for floor: Int) -> [WayfindingEdge] {
        return edges.filter({ edge in
            let nodes = self.nodes(for: edge)
            return nodes.0.floor == floor || nodes.1.floor == floor
        })
    }
    
    var currentRoute: CustomRoute? = nil
    
    private var _routeHistory: [WayfindingNode]!
    private(set) var routeHistory:  [WayfindingNode] {
        get {
            if _routeHistory == nil {
                _routeHistory = []
            }
            return _routeHistory
        } set {
            _routeHistory = newValue
        }
    }
    
    private var nodesDistances: [String: Double]! = [:]
    private var currentLocation: Coordinates!
    
    
    class func parse(json jsonName: String) -> WayfindindGraph? {
        guard let path = Bundle.main.path(forResource: jsonName, ofType: "json") else { return nil }
        guard let jsonData = try? Data(contentsOf: URL(fileURLWithPath: path)) else { return nil }
        
        let graph = try? JSONDecoder().decode(WayfindindGraph.self, from: jsonData)
        graph?.edges = graph!.edges.map({ $0.reordered() })
        graph?.nodes.enumerated().forEach({$0.1.index = $0.0})
        graph?.restoreMissedEdges()
        graph?.calculateDistances()
        
        return graph
    }
    
    func reset() {
        routeHistory = []
    }
    
    
    func length(for edge: WayfindingEdge) -> Double {
        let edgeNodes =  nodes(for: edge)
        return edgeNodes.0.distance(to: edgeNodes.1)
    }
    
    
    func getRoute(to location: CLLocationCoordinate2D, floor: Int?, completion: @escaping (CustomRoute?)->Void) {
        var current: Coordinates! = currentLocation
        
        let coordinates = Coordinates(lat: location.latitude, lon: location.longitude, floor: floor)
        let route = CustomRoute()
        route.start = current
        route.end = coordinates
        route.edges = getRouteEdges(to: nodesSortedByDistance(to: coordinates).first)
        
        
        if current == nil {
            
            guard let currentLocationCoordinate = CHLocationManager.shared.clManager.location?.coordinate else { return }
            current = Coordinates(coordinate: currentLocationCoordinate, floor: floor)
            
            guard let nearestEntrance = MainService.shared.mainLocation?.entrances.min(by: { first, second in
                let firstLocation = CLLocation(latitude: first.coordinates.lat, longitude: first.coordinates.lon)
                let secondLocation = CLLocation(latitude: second.coordinates.lat, longitude: second.coordinates.lon)
                let currentLocation = CLLocation(latitude: current.lat, longitude: current.lon)
                return firstLocation.distance(from: currentLocation) < secondLocation.distance(from: currentLocation)
            }) else { return }
            
            route.entrance = nearestEntrance
            route.start = nearestEntrance.coordinates
            route.edges = getRouteEdges(from: nearestEntrance, to: nodesSortedByDistance(to: coordinates).first)
            
            Alamofire.request("https://maps.googleapis.com/maps/api/directions/json", parameters: [
                "origin": "\(current!.lat!),\(current!.lon!)",
                "destination": "\(nearestEntrance.coordinates!.lat!),\(nearestEntrance.coordinates!.lon!)",
                "key": "AIzaSyADYvPVirG77cBgRtb_B2i2oTA4PZ3cM9E",
                "sensor": "true",
                "mode": "driving"
                ]).responseJSON { (response) in
                
                    if let json = response.result.value as? NSDictionary, let routes = (json["routes"] as? [NSDictionary])?.first {
                        route.encodedGMSPath = (routes["overview_polyline"] as? NSDictionary)?["points"] as? String
                        route.gmsDistance = ((routes["legs"] as? [NSDictionary])?.first?["distance"] as? NSDictionary)?["value"] as? Double ?? 0
                    }
                    
                    self.currentRoute = route
                    completion(self.currentRoute)
            }
        } else {
            route.edges = getRouteEdges(to: nodesSortedByDistance(to: coordinates).first)
            self.currentRoute = route
            completion(self.currentRoute)
        }
        
       
        
    }
    
    
    
    func onArrived(to _location: CLLocationCoordinate2D, floor: Int!) -> CLLocationCoordinate2D {
        guard floor != nil else { return _location }
        
        var location = _location
        let coordinates = Coordinates(lat: location.latitude, lon: location.longitude, floor: floor)
        
        guard let nearestNode = nodesSortedByDistance(to: coordinates).first else { return location }
        onArrived(to: nearestNode)
        
        let route = getRouteEdges(to: nearestNode)
        
        if route.count != 0 {
            if route.count == 1 {
                let currentNodes = nodes(for: route.first!)
                location = location.intersectionWithLine(currentNodes.0.coordinate, currentNodes.1.coordinate)
            } else {
                for edge in route {
                    let currentNodes = nodes(for: edge)
                    if edge == route.last {
                        return onArrived(to: currentNodes.1.coordinate, floor: floor)
                    } else {
                        _ = onArrived(to: currentNodes.1.coordinate, floor: floor)
                    }
                }
            }
        }
        
        
        if route.count > 1, let nearestPoint = pointOnGraph(for: location, nearestNode: nearestNode, floor: floor) {
            location = nearestPoint
        }
        
        currentLocation = Coordinates(coordinate: location, floor: floor)
        currentRoute?.locationChanged(to: currentLocation)
        
        return location
    }
    
    
    func nodes(for edge: WayfindingEdge) -> (WayfindingNode, WayfindingNode) {
        return (nodes[edge.begin], nodes[edge.end])
    }
    
    
    
    
    func nodesSortedByDistance(to coordinate: Coordinates)  -> [WayfindingNode] {
        return nodes.filter({ $0.floor == coordinate.floor }).sorted(by: { $0.distance(to: coordinate.clCoordinates) < $1.distance(to: coordinate.clCoordinates) })
    }
}

private extension WayfindindGraph {
    
    func execute(source: WayfindingNode) {
        settledNodes = Set<WayfindingNode>()
        unSettledNodes = Set<WayfindingNode>()
        distance = [:]
        predecessors = [:]
        distance[source] =  0.0
        unSettledNodes.insert(source)
        while (unSettledNodes.count > 0) {
            if let node = getMinimum(nodes: unSettledNodes) {
                settledNodes.insert(node)
                unSettledNodes.remove(node)
                findMinimalDistances(node: node)
            }
        }
    }
    
    func findMinimalDistances(node: WayfindingNode) {
        let adjacentNodes = getNeighbors(node: node)
        for target in adjacentNodes {
            if (getShortestDistance(destination: target) > getShortestDistance(destination: node) + getDistance(node: node, target: target)) {
                distance[target] = getShortestDistance(destination: node) + getDistance(node: node, target: target)
                predecessors[target] = node
                unSettledNodes.insert(target)
            }
        }
    }
    
    func getDistance(node: WayfindingNode, target: WayfindingNode) -> Double {
        let i = node.index!
        let j = target.index!
        return nodesDistances!["\(min(i, j))-\(max(i, j))"]!
    }
    
    func getNeighbors(node: WayfindingNode) -> [WayfindingNode] {
        var neighbors: [WayfindingNode] = []
        for edge in edges {
            if (edge.begin == node.index && !isSettled(vertex: nodes[edge.end])) {
                neighbors.append(nodes[edge.end])
            }
        }
        return neighbors
    }
    
    func getMinimum(nodes: Set<WayfindingNode>) -> WayfindingNode? {
        var minimum:WayfindingNode? = nil
        for vertex in nodes {
            if (minimum == nil) {
                minimum = vertex
            } else {
                if (getShortestDistance(destination: vertex) < getShortestDistance(destination: minimum!)) {
                    minimum = vertex
                }
            }
        }
        return minimum
    }
    
    func isSettled(vertex: WayfindingNode) -> Bool {
        return settledNodes.contains(vertex)
    }
    
    func getShortestDistance(destination: WayfindingNode) -> Double {
        let d = distance[destination]
        if (d == nil) {
            return Double.infinity
        } else {
            return d!
        }
    }
    
    func getPath(target: WayfindingNode) -> [WayfindingEdge]? {
        var path: [WayfindingNode] = []
        var step = target
        // check if a path exists
        if (predecessors[step] == nil) {
            return nil
        }
        path.append(step)
        while (predecessors[step] != nil) {
            step = predecessors[step]!
            path.append(step)
        }
        // Put it into the correct order
        path.reverse()
        
        var edges: [WayfindingEdge] = []
        
        var i = 0
        while i < path.count - 1 {
            let edge = WayfindingEdge.init()
            edge.begin = path[i].index
            edge.end = path[i + 1].index
            edges.append(edge)
            
            i = i + 1
        }
        return edges
    }
    
    func restoreMissedEdges() {
        edges.append(contentsOf: edges.map({
            let newEdge = WayfindingEdge.init()
            newEdge.begin = $0.end
            newEdge.end = $0.begin
            return newEdge
        }))
//        for _ in 0..<nodes.count {
//
//            var newEdges: [WayfindingEdge] = []
//            let _edges = edges
//
//            for edge in _edges {
//                for node in nodes {
//                    let edgeNodes = nodes(for: edge)
//                    if edgeNodes.0.index != node.index && edgeNodes.1.index != node.index {
//                        if isNode(node, on: edge) {
//                            if !edges.contains(where: { ($0.begin == node.index && $0.end == edge.begin) || ($0.begin == edge.begin && $0.end == node.index) }) {
//
//                                let newEdge = WayfindingEdge.init()
//                                newEdge.begin = min(node.index, edge.begin)
//                                newEdge.end = max(node.index, edge.begin)
//                                newEdges.append(newEdge)
//
//                            }
//
//                            if !edges.contains(where: { $0.begin == node.index && $0.end == edge.end || $0.begin == edge.end && $0.end == node.index }) {
//
//                                let newEdge = WayfindingEdge.init()
//                                newEdge.begin = min(node.index, edge.end)
//                                newEdge.end = max(node.index, edge.end)
//                                newEdges.append(newEdge)
//                            }
//
//                            edges.removeAll(where: { $0.begin == edge.begin && $0.end == edge.end })
//                        }
//                    }
//                }
//            }
//
//            edges += newEdges
//
//            if newEdges.count == 0 {
//                break;
//            }
//        }
        
    }
    
    func calculateDistances() {
        
        if nodesDistances == nil  {
            nodesDistances = [:]
        }
        
        for i in 0..<nodes.count {
            for j in 0..<nodes.count {
                if i != j && !(nodesDistances?.keys.contains("\(i)-\(j)") ?? false ||  nodesDistances?.keys.contains("\(j)-\(i)")  ?? false) {
                    nodesDistances?["\(min(i, j))-\(max(i, j))"] = nodes[i].distance(to: nodes[j])
                }
            }
        }
    }
    
    
    func isNode(_ node: WayfindingNode, on edge: WayfindingEdge) -> Bool {
        let (begin, end) = nodes(for: edge)
        
        guard begin.floor == end.floor && end.floor == node.floor else { return false }
        
        let beginDistance = begin.distance(to: node.location.coordinate)
        let endDistance = end.distance(to: node.location.coordinate)
        let edgeLength = begin.distance(to: end.location.coordinate)
        return abs(beginDistance + endDistance - edgeLength) <= 0.15
    }
    
    
    func nearestNodes(for node: WayfindingNode) -> [WayfindingNode] {
        var allDistances: [Int: Double] = [:]
        
        nodesDistances.filter({ $0.key.contains(String(node.index)) }).forEach({ (key, value) in
            let ids = key.components(separatedBy: "-")
            
            var otherNodeID: String!
            
            if ids.first! != String(node.index) {
                otherNodeID = ids.first!
            } else {
                otherNodeID = ids.last!
            }
            
            allDistances[Int(otherNodeID)!] = value
        })
        
        return allDistances.sorted(by: {$0.value < $1.value }).map({ nodes[$0.key] })
    }
    
    
    func conjoinedNodes(for node: WayfindingNode) -> [WayfindingNode] {
        var result: [WayfindingNode?] = []
        
        for edge in edges.filter({$0.begin == node.index || $0.end == node.index }) {
            if edge.begin != node.index {
                result.append(nodes.first(where: {$0.index == edge.begin}))
            } else {
                result.append(nodes.first(where: {$0.index == edge.end}))
            }
        }
        return  result.compactMap({$0})
    }
    
    func conjoinedEdges(for node: WayfindingNode) -> [WayfindingEdge] {
        return edges.filter({ $0.begin == node.index || $0.end == node.index })
    }
    
    func onArrived(to node: WayfindingNode!) {
        guard let node = node else { return }

        if routeHistory.last?.index != node.index {
            routeHistory.append(node)
        }
    }
    
    
    
    func lastDirection() -> CLLocationDegrees? {

        guard case let lastPoints = routeHistory.suffix(2), lastPoints.count == 2 else { return nil }
        return lastPoints.first!.location.bearing(to: lastPoints.last!.location)
    }
    
    
    func nextPointPredictedByDirection() -> WayfindingNode? {
        guard let last = routeHistory.last, let lastDirection = lastDirection() else { return nil }
        
        return nodes.filter({ $0.index != last.index && abs(last.bearingDelta(to: $0, with: lastDirection)) < 30 }).min(by: { last.distance(to: $0) < last.distance(to: $1) })
    }
    
    func currentEdge() -> WayfindingEdge? {
        guard let next = nextPointPredictedByDirection() else { return nil }
        return edges.first(where: {$0.begin == next.index && $0.end == routeHistory.last!.index || $0.end == next.index && $0.begin == routeHistory.last!.index})
    }
    
    func topIntersections(with coordinate: CLLocationCoordinate2D, floor: Int) -> [CLLocationCoordinate2D] {

        var possibleEdges = edges
        
        possibleEdges = possibleEdges.filter({ edge in
            let nodes = self.nodes(for: edge)
            return nodes.0.floor == floor || nodes.1.floor == floor
        })

        let edgeIntersections: [CLLocationCoordinate2D] = possibleEdges.map({
            let edgeNodes = self.nodes(for: $0)
            return coordinate.intersectionWithLine(edgeNodes.0.coordinate, edgeNodes.1.coordinate)
        })
    
        let sortedIntersections = edgeIntersections.sorted(by: {
            coordinate.distance(to: $0) < coordinate.distance(to: $1)
        })
        return sortedIntersections
    }
    
    func intersection(with coordinate: CLLocationCoordinate2D, floor: Int) -> CLLocationCoordinate2D! {
        return topIntersections(with: coordinate, floor: floor).first
    }
    
    func pointOnGraph(for coordinate: CLLocationCoordinate2D, nearestNode: WayfindingNode, floor: Int) -> CLLocationCoordinate2D! {
        
        guard let last = routeHistory.last else { return intersection(with: coordinate, floor: floor) }
        
        let route = routeEdges(from: last, to: nearestNode)
        
        
        let possibleNodes = conjoinedNodes(for: last)
        
        let nearestNode = possibleNodes.min(by: {
            $0.coordinate.distance(to: coordinate) < $1.coordinate.distance(to: coordinate)
        })
        
        if let edge = edges.first(where: { $0.begin == nearestNode?.index && $0.end == last.index || $0.end == nearestNode?.index && $0.begin == last.index }) {
            
            let edgeNodes = nodes(for: edge)
            
            return coordinate.intersectionWithLine(edgeNodes.0.coordinate, edgeNodes.1.coordinate)
        }
        
        return intersection(with: coordinate, floor: floor)
        
    }
 
    
    
}


//Routing
private extension WayfindindGraph {
    
    
    func getRouteEdges(to targetNode: WayfindingNode!) -> [WayfindingEdge] {
        guard let targetNode = targetNode, let last = routeHistory.last else { return [] }
        return routeEdges(from: last, to: targetNode)
    }
    
    func getRouteEdges(from entrance: BuildingEntrance, to targetNode: WayfindingNode!) -> [WayfindingEdge] {
        guard let lastNode = nodesSortedByDistance(to: entrance.coordinates).first else { return [] }
        guard let targetNode = targetNode else { return [] }
        
        return routeEdges(from: lastNode, to: targetNode)
    }
    
    func routeEdges(from sourceNode: WayfindingNode, to targetNode: WayfindingNode) -> [WayfindingEdge] {
        return routeEdgesInternalDijkstra(from: sourceNode, to: targetNode)
    }
    
    func routeEdgesInternalDijkstra(from sourceNode: WayfindingNode,
                                    to targetNode: WayfindingNode) -> [WayfindingEdge] {
        execute(source: sourceNode)
        return getPath(target: targetNode) ?? []
    }
    
    func routeEdgesInternal(from sourceNode: WayfindingNode,
                            to targetNode: WayfindingNode,
                            history: [WayfindingNode]) -> [WayfindingEdge] {
        //ноды, которые соединены ребром с заданной нодой
        let sourceConjoined = conjoinedNodes(for: sourceNode)
        
        if sourceConjoined.contains(where: { $0 == targetNode }) {
            var newHistory = history
            
            if !newHistory.contains(where: { $0 == sourceNode }) {
                newHistory.append(sourceNode)
            }
            
            if !newHistory.contains(where: { $0 == targetNode }) {
                newHistory.append(targetNode)
            }
            
            var resultEdges: [WayfindingEdge] = []
            
            for i in 0..<(newHistory.count - 1) {
                let edge = WayfindingEdge.init()
                edge.begin = newHistory[i].index
                edge.end = newHistory[i + 1].index
                resultEdges.append(edge)
            }
            return resultEdges
        }
        
        var paths: [[WayfindingEdge]] = []
        
        let conjoinedAndNotVisited = sourceConjoined.filter({ item in !history.contains(where: { $0 == item  })
        })
        for node in conjoinedAndNotVisited {
            var newHistory = history
            
            if !newHistory.contains(where: { $0 == sourceNode }) {
                newHistory.append(sourceNode)
            }
            
            let results = routeEdgesInternal(from: node, to: targetNode, history: newHistory)
            print("results count: \(results.count)")
            paths.append(results)
        }
        
        paths = paths.filter{( $0.count != 0 )}
        if (paths.count == 0) {
            return []
        } else if (paths.count == 1) {
            return paths[0]
        } else {
            return paths.min(by: { firstEdges, secondEdges in
                //суммируются длины все ребер в метрах
                return  firstEdges.reduce(0, { $0 + self.length(for: $1) }) < secondEdges.reduce(0, { $0 + self.length(for: $1) })
            }) ?? []
        }
        
    }

    
}



