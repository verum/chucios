//
//  WayfindingEdge.swift
//  CHUC K
//
//  Created by Константин on 06/03/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import Foundation


class WayfindingEdge: Codable, Equatable {
    var begin: Int!
    var end: Int!
    
    
    static func == (lhs: WayfindingEdge, rhs: WayfindingEdge) -> Bool {
        let lReorderd = lhs.reordered()
        let rReordered = rhs.reordered()
        return lReorderd.begin == rReordered.begin && lReorderd.end == rReordered.end
    }
    
    
    func reordered() -> WayfindingEdge {
        let value = self
        let _begin = min(begin, end)
        let _end = max(begin, end)
        value.begin = _begin
        value.end = _end
        return value
    }
}
