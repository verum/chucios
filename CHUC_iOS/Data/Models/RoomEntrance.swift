//
//  Entrance.swift
//  CHUC K
//
//  Created by Константин on 26/02/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import Foundation
import CoreLocation


class RoomEntrance: Codable {

    var lat: Double = 0
    var lon: Double = 0
    
    var coordinate: CLLocationCoordinate2D {
        get {
            return CLLocationCoordinate2D(latitude: lat, longitude: lon)
        }
    }

}
