//
//  Floor.swift
//  CHUC K
//
//  Created by Константин on 26/02/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import Foundation



class Floor: Codable, Equatable, Comparable {
    
    static func < (lhs: Floor, rhs: Floor) -> Bool {
        return lhs.level < rhs.level
    }
    
    static func == (lhs: Floor, rhs: Floor) -> Bool {
        return lhs.level == rhs.level
    }
    
    var planID = ""
    var level: Int = 1
    var rooms: [Room] = []
    
    var width: Double = 0.0
    var height: Double = 0.0
    var bearing: Double = 0.0
    var bitmapWidth: Int = 0
    var bitmapHeight: Int = 0
    var url: String = ""
 
    var center: Coordinates
}
