//
//  Polyline.swift
//  CHUC K
//
//  Created by Константин on 06/03/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import Foundation
import GoogleMaps

class Polyline: GMSPolyline {
    
    var start: Int?
    var end: Int?
    
    convenience init(start s: Int?, end e: Int?, path p: GMSPath) {
        self.init(path: p)
        start = s
        end = e
    }
}
