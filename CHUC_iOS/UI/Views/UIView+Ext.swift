//
//  UIView_Extensions.swift
//  CHUC_iOS
//
//  Created by Константин on 22/01/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import UIKit


@IBDesignable extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
           return self.layer.cornerRadius
        } set {
            self.layer.cornerRadius = newValue
            self.layer.masksToBounds = newValue != 0
        }
    }
    
    @IBInspectable var borderColor:UIColor? {
        set {
            layer.borderColor = newValue!.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor:color)
            }
            else {
                return nil
            }
        }
    }
    @IBInspectable var borderWidth:CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    
    enum safeInsetTypes {
        case top
        case bottom
        case left
        case right
    }
    
    func safeArea(_ edge: safeInsetTypes) -> CGFloat {
        if #available(iOS 11.0, *) {
            switch edge {
            case .top: return self.safeAreaInsets.top
            case .bottom: return self.safeAreaInsets.bottom
            case .left: return self.safeAreaInsets.left
            case .right: return self.safeAreaInsets.right
            }
        }  else {
            switch edge {
            case .top: return UIApplication.shared.statusBarFrame.height
            case .bottom: return 0
            case .left: return 0
            case .right: return 0
            }
        }
    }
}


extension UIImage {
    
    func scale(toWidth width:CGFloat) -> UIImage? {
        
        let oldWidth = self.size.width
        let oldHeight = self.size.height
        
        var scaleFactor:CGFloat
        var newHeight:CGFloat
        var newWidth:CGFloat
        
        if oldWidth > oldHeight {
            
            scaleFactor = oldHeight/oldWidth
            newHeight = width * scaleFactor
            newWidth = width
        } else {
            
            scaleFactor = oldHeight/oldWidth
            newHeight = width * scaleFactor
            newWidth = width
        }
        
        UIGraphicsBeginImageContext(CGSize(width:newWidth, height:newHeight))
        self.draw(in: CGRect(x:0, y:0, width:newWidth, height:newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
        
    }
    
}
