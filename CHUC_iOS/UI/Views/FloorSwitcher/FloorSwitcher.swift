//
//  FloorSwitcher.swift
//  CHUC K
//
//  Created by Константин on 28/02/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import UIKit


class FloorSwitcher: UIView {
    
    weak var delegate: FloorSwitcherDelegate?

    class func make(location: Location?, selectedFloor: Floor?, delegate: FloorSwitcherDelegate?) -> FloorSwitcher {
        let new = UINib(nibName: "FloorSwitcher", bundle: nil).instantiate(withOwner: nil, options: nil).first as! FloorSwitcher
        new.location = location
        new.delegate = delegate
        new.selectedFloor = selectedFloor
        return new
    }
    
    var location: Location!
    var selectedFloor: Floor! {
        didSet {
            tableView?.reloadData()
        }
    }
    
    var requiredHeight: CGFloat {
        get {
            return CGFloat(location?.floors.count ?? 0) * 50 + 16
        }
    }
    
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        self.backgroundColor = UIColor.black.withAlphaComponent(0.15)
    }
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            FSTableCell.register(in: tableView)
        }
    }
    
}


extension FloorSwitcher: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return location?.floors.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FSTableCell.id, for: indexPath) as! FSTableCell
        let currentFloor = location.floors[indexPath.row]
        cell.update(floor: currentFloor, isSelected: currentFloor.planID == selectedFloor?.planID)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentFloor = location.floors[indexPath.row]
        guard selectedFloor?.planID != currentFloor.planID else { return }
        selectedFloor = currentFloor
        tableView.reloadData()
        delegate?.floorSwitched(to: currentFloor)

    }
    
    
}
