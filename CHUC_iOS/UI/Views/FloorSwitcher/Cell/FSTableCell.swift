//
//  FSTableCell.swift
//  CHUC K
//
//  Created by Константин on 28/02/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import UIKit


class FSTableCell: UITableViewCell {
    
    static let id = "FSTableCell"

    class func register(in tableView: UITableView) {
        tableView.register(UINib(nibName: "FSTableCell", bundle: nil), forCellReuseIdentifier: id)
    }
    
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        self.contentView.backgroundColor = .clear
        self.backgroundColor = .clear
        self.selectionStyle = .none
    }
    
    @IBOutlet weak var circle: UIView!
    @IBOutlet weak var label: UILabel!

    private var floor: Floor?
    
    func update(floor _floor: Floor, isSelected: Bool) {
        floor = _floor
        label.text = String(_floor.level)
        circle.borderColor = isSelected ? .mainColor: UIColor.lightGray.withAlphaComponent(0.5)
        circle.alpha = isSelected ? 1 : 0.7
        label.font = UIFont.systemFont(ofSize: 17, weight: isSelected ? .bold : .light)
    }
    
    
    
}


extension UIColor {
    
    convenience public init(hex: String!) {
        guard hex != nil else { self.init(red: 0, green: 0, blue: 0, alpha: 0); return }
        
        let color: UIColor = {
            var cString:String = hex.trimmingCharacters(in: (NSCharacterSet.whitespacesAndNewlines as NSCharacterSet) as CharacterSet).uppercased()
            
            if (cString.hasPrefix("#")) {
                cString = String(cString.dropFirst())
            }
            
            if ((cString.count) != 6) {
                return UIColor.gray
            }
            
            var rgbValue:UInt32 = 0
            Scanner(string: cString).scanHexInt32(&rgbValue)
            
            return UIColor(
                red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                alpha: CGFloat(1.0)
            )
        }()
        
        self.init(cgColor: color.cgColor)
    }
    
    static let mainColor = UIColor(hex: "F0BC6B")
    
}
