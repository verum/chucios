//
//  MapLabelView.swift
//  CHUC K
//
//  Created by Константин on 26/02/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import UIKit

class MapLabelView: UIView {

    class func make(_ text: String) -> MapLabelView {
        let instance = UINib(nibName: "MapLabelView", bundle: nil).instantiate(withOwner: nil, options: nil).first as! MapLabelView
        instance.label.text = text
        instance.frame.size = instance.systemLayoutSizeFitting(.zero)
        return instance
    }
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var labelView: UIView!
    
}
