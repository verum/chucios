//
//  ViewController.swift
//  CHUC_iOS
//
//  Created by Константин on 22/01/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
import IndoorAtlas


class MapViewController: UIViewController {
    
    
    lazy var chManager: CHLocationManager = {
        return CHLocationManager.shared
    }()
    
    var clManager: CLLocationManager {
        get {
            return chManager.clManager
        }
    }
    
    lazy var iaLocationMarker: GMSMarker = {
        let m = GMSMarker(position: CLLocationCoordinate2D(latitude: 0, longitude: 0))
        m.tracksViewChanges = false
        m.layer.backgroundColor = UIColor.black.cgColor
        m.iconView = UIView()
        m.iconView?.layer.borderWidth = 2
        m.iconView?.layer.borderColor = UIColor.white.cgColor
        m.iconView?.frame.origin = CGPoint(x: 10, y: 10)
        m.iconView?.frame.size = CGSize(width: 20, height: 20)
        m.iconView?.cornerRadius = 10
        m.iconView?.backgroundColor = UIColor.green.withAlphaComponent(1)
        m.map = mapView
        m.zIndex = 102
        return m
    }()
    
    lazy var iaLocationZone: GMSMarker = {
        let c = GMSMarker(position: CLLocationCoordinate2D(latitude: 0, longitude: 0))
        c.tracksViewChanges = false
        c.isDraggable = true
        
        let view = UIView()
        view.backgroundColor = UIColor.purple.withAlphaComponent(0.5)
        view.frame = CGRect(origin: .zero, size: CGSize(width: 64, height: 64))
        view.cornerRadius = 32
        c.iconView = view
        
        c.map = mapView
        c.zIndex = 101
        return c
    }()
    
    
    fileprivate var overlays: [GMSGroundOverlay] = []
    fileprivate var wasAnimated = false
    fileprivate var lines: [GMSPolyline] = []
    fileprivate var markers: [GMSMarker] = []
    fileprivate var directionMarker: GMSMarker? {
        didSet {
            if directionMarker == nil {
                routeWasBuilt = false
                routeLevel = nil
            }
        }
    }
    fileprivate var routeWasBuilt = false
    fileprivate var routeLevel: Int? = nil
    
    @IBOutlet weak var mapView: GMSMapView! {
        didSet {
            mapView?.isTrafficEnabled = false
            mapView?.isBuildingsEnabled = true
            mapView?.isMyLocationEnabled = true
            mapView?.isIndoorEnabled = false
            mapView?.delegate = self
            if let url =   Bundle.main.url(forResource: "mapStyle", withExtension: "json") {
                mapView.mapStyle = try? GMSMapStyle(contentsOfFileURL: url)
            }
        }
    }
    
    @IBOutlet weak var compassIcon: UIImageView!
    @IBOutlet weak var routeCancelButton: UIButton!
    @IBOutlet weak var routeTitleLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var floorView: UIView!
    @IBOutlet weak var floorLabel: UILabel!
    
    var currentFloor: Floor? = nil
    
    var selectedFloor: Floor! = {
        return MainService.shared.mainLocation?.floors.min()
        }() {
        didSet {
            changeOverlay()
            floorSwitcher.selectedFloor = selectedFloor
            redrawGraph()
            updateLocationMarker()
            if let route = chManager.graph?.currentRoute {
                redrawCustomRoute(route)
            }
        }
    }
    
    var edgeLines: [Polyline] = []
    var nodes: [GMSCircle] = []
    
    var roomLabels: [GMSMarker] = []
    var focusedRoom: Room? {
        didSet {
            roomLabels.forEach({
                let iconView = $0.iconView as? MapLabelView
                iconView?.labelView.backgroundColor = UIColor.white.withAlphaComponent(0.75)
                iconView?.label.textColor = .black
                $0.iconView = nil
                $0.iconView = iconView
                $0.zIndex = 100
            })
            
            guard let coordinate = focusedRoom?.title?.coordinate else { return }
            if let labelMarker = roomLabels.first(where: { $0.position == coordinate }) {
                let iconView = labelMarker.iconView as? MapLabelView
                iconView?.labelView?.backgroundColor = UIColor.mainColor
                iconView?.label.textColor = .white
                labelMarker.iconView = nil
                labelMarker.iconView = iconView
                labelMarker.zIndex = 1000
            }
        }
    }
    
    lazy var floorSwitcher: FloorSwitcher = {
        return FloorSwitcher.make(location: MainService.shared.mainLocation, selectedFloor: selectedFloor, delegate: self)
    }()
    
    var nodeMarkers: [GMSMarker] = []
    enum modes {
        case normal
        case route
    }
    
    private var mode: modes = .normal {
        didSet {
            routeCancelButton.isHidden = mode != .route
            routeTitleLabel.isHidden = mode != .route
            if mode == .normal {
                routeTitleLabel.text = "Pick a destination on the floor map"
            }
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        chManager.delegate = self
        chManager.startUpdatingLocation()
        
        clManager.delegate = self
        
        if CLLocationManager.authorizationStatus() == .notDetermined {
            clManager.requestWhenInUseAuthorization()
        } else {
            clManager.startUpdatingLocation()
            clManager.startUpdatingHeading()
        }
        
        
        
        
        MainService.shared.subscribeForPlans { [weak self] in
            guard let `self` = self else { return }
            
            async {
                if self.floorSwitcher.superview == nil {
                    self.floorSwitcher.frame = CGRect(x: self.view.bounds.width - 64, y: self.view.bounds.height - self.floorSwitcher.requiredHeight - 20 - self.view.safeArea(.bottom), width: 60, height: self.floorSwitcher.requiredHeight)
                    self.view.addSubview(self.floorSwitcher)
                    self.view.bringSubviewToFront(self.floorSwitcher)
                }
            }
            
            self.changeOverlay()
            
        }
        
        redrawGraph()
    }
    
    
    
    
    func redrawGraph() {
        
        nodes.forEach({ $0.map = nil })
        nodes = []
        
        edgeLines.forEach({ $0.map = nil })
        edgeLines = []
        
        
        nodeMarkers.forEach({ $0.map = nil })
        nodeMarkers = []
        
        if let _ = chManager.graph {
            
            for entrance in MainService.shared.mainLocation?.entrances ?? [] {
                let labelMarker = GMSMarker(position: entrance.title.coordinate)
                labelMarker.tracksViewChanges = false
                let label = UITextView()
                label.textContainerInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
                label.backgroundColor = .red
                label.textColor = .white
                label.text = String(entrance.title.text)
                label.sizeToFit()
                labelMarker.iconView = label
                labelMarker.map = mapView
            }
            
//            for node in graph.nodes.filter({ $0.floor == selectedFloor.level }) {
//
//                let circle = GMSCircle(position: node.coordinate, radius: 1/4)
//                circle.fillColor = .red
//                circle.strokeWidth = 2
//                circle.strokeColor = .orange
//                circle.map = mapView
//                circle.zIndex = 205
//
//                nodes.append(circle)
//
//                let labelMarker = GMSMarker(position: node.coordinate)
//                labelMarker.tracksViewChanges = false
//                let label = UILabel()
//                label.numberOfLines = 0
//                label.text = String(node.index)
//                label.sizeToFit()
//                labelMarker.iconView = label
//                labelMarker.map = mapView
//
//                nodeMarkers.append(labelMarker)
//            }
//
//            for edge in graph.edges.filter({
//                let nodes = graph.nodes(for: $0)
//                return nodes.0.floor == selectedFloor.level && nodes.1.floor == selectedFloor.level
//            })  {
//                let firstNode = graph.nodes[edge.begin]
//                let secondNode = graph.nodes[edge.end]
//
//                let path = GMSMutablePath()
//                path.addLatitude(firstNode.latitude, longitude: firstNode.longitude)
//                path.addLatitude(secondNode.latitude, longitude: secondNode.longitude)
//
//                let polyline = Polyline(start: edge.begin, end: edge.end, path: path)
//                polyline.strokeColor = .orange
//                polyline.strokeWidth = 2
//                polyline.map = mapView
//                polyline.zIndex = 104
//                edgeLines.append(polyline)
//
//            }
        }
    }
    
    func changeOverlay() {
        
        for floor in (MainService.shared.mainLocation?.floors ?? []) {
            
            if floor.level == self.selectedFloor.level {
                
                if let info = MainService.shared.images[floor.level] {
                    self.makeOverlay(with: info.1, plan: info.0)
                }
                
                self.roomLabels.forEach({ $0.map = nil })
                self.roomLabels = []
                
                for room in floor.rooms {
                    
                    if let title = room.title {
                        
                        let labelMarker = GMSMarker(position: title.coordinate)
                        labelMarker.tracksViewChanges = false
                        
                        let labelView = MapLabelView.make(title.text)
                        
                        if room == self.focusedRoom {
                            labelView.labelView?.backgroundColor = UIColor.mainColor
                            labelView.label.textColor = .white
                            labelMarker.zIndex = 1000
                        }
                        
                        labelMarker.iconView = labelView
                        labelMarker.map = self.mapView
                        
                        self.roomLabels.append(labelMarker)
                        
                        
                    }
                }
            }
        }
        
    }
    
    
    
    
    
    func redrawCustomRoute(_ route: CustomRoute, nodeChanged: Bool = true) {
        
        let routeLength = route.length(in: self.chManager.graph)
        
        if routeLength != 0 {
            
            routeTitleLabel.text = "~ " + routeLength.distanceString + " left"
        }
        
        let routeIsTooShort = routeLength < 3
        
        if routeIsTooShort && routeWasBuilt {
            
            let alert = UIAlertController(title: "Here you are!", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            directionMarker?.map = nil
            directionMarker = nil
            mode = .normal
            return;
        }
        
        
        if !routeIsTooShort {
            routeWasBuilt = true
            directionMarker?.map = nil
            directionMarker = GMSMarker(position: route.end.clCoordinates)
            directionMarker?.tracksViewChanges = false
            directionMarker?.map = mapView
        }
        
        
        
        if route.edges.count != 0, let firstNode = chManager.graph?.nodes(for: route.edges.first!).0 {
            
            let beginningPath =  GMSMutablePath()
            beginningPath.add(route.start.clCoordinates)
            beginningPath.add(firstNode.coordinate)
            
            let beginningPolyLine = GMSPolyline(path: beginningPath)
            beginningPolyLine.strokeWidth = 3
            beginningPolyLine.map = mapView
            beginningPolyLine.zIndex = 203
            beginningPolyLine.strokeColor = UIColor.blue.withAlphaComponent(firstNode.floor == selectedFloor.level ? 1 : 0.3)
            

            lines.insert(beginningPolyLine, at: 0)
        }
        
        
        if route.edges.count != 0, let lastNode = chManager.graph?.nodes(for: route.edges.last!).1 {
            
            let endingPath =  GMSMutablePath()
            endingPath.add(lastNode.coordinate)
            endingPath.add(route.end.clCoordinates)
            
            let endingPolyLine = GMSPolyline(path: endingPath)
            endingPolyLine.strokeWidth = 3
            endingPolyLine.map = mapView
            endingPolyLine.zIndex = 203
            endingPolyLine.strokeColor = UIColor.blue.withAlphaComponent(lastNode.floor == selectedFloor.level ? 1 : 0.3)
            
            lines.append(endingPolyLine)
        }
        
        
        guard nodeChanged else { return }
        
        for (index, line) in lines.enumerated() {
            if index != 0 && index != lines.count - 1 {
                line.map = nil
            }
        }
        
        lines = [lines.first, lines.last].compactMap({ $0 })
        
        for leg in route.edges  {
            
            if let nodes = chManager.graph?.nodes(for: leg) {
                let path =  GMSMutablePath()
                path.add(nodes.0.coordinate)
                path.add(nodes.1.coordinate)
                
                let polyLine = GMSPolyline(path: path)
                polyLine.strokeWidth = 3
                polyLine.map = mapView
                polyLine.zIndex = 203
                
                lines.append(polyLine)
                
                let floors: [Int] = [nodes.0.floor, nodes.1.floor]
                
                if floors.first == selectedFloor.level && floors.last == selectedFloor.level {
                    polyLine.strokeColor = UIColor.blue
                } else if floors.first == selectedFloor.level || floors.last == selectedFloor.level {
                    
                    polyLine.strokeColor = .clear
                    
                    let styles: [GMSStrokeStyle] = [.solidColor(.blue), .solidColor(.clear)]
                    let scale = 1.0 / mapView.projection.points(forMeters: 1, at: mapView.camera.target)
                    let solidLine = NSNumber(value: 8.0 * Float(scale))
                    let gap = NSNumber(value: 4.0 * Float(scale))
                    polyLine.spans = GMSStyleSpans(polyLine.path!, styles, [solidLine, gap], GMSLengthKind.rhumb)
                } else {
                    polyLine.strokeColor = UIColor.blue.withAlphaComponent(0.3)
                }
                
                if nodes.0.floor != nodes.1.floor {
                    let changeFloorMarker = GMSMarker(position: nodes.1.coordinate)
                    let label = UITextView()
                    let text: String
                    let arg = "\(nodes.1.floor!)"
                    if nodes.0.floor > nodes.1.floor {
                        text = String.localizedStringWithFormat(NSLocalizedString("Go down to floor", comment: ""), arg)
                    } else {
                        text = String.localizedStringWithFormat(NSLocalizedString("Go up to floor", comment: ""), arg)
                    }
                    label.text = text
                    label.textColor = UIColor.black
                    label.backgroundColor = UIColor.white
                    label.textContainerInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
                    label.sizeToFit()
                    changeFloorMarker.iconView = label
                    changeFloorMarker.map = mapView
                    
                    markers.append(changeFloorMarker)
                }
                
            }
        }
        
        if let encoded = route.encodedGMSPath {
            if let path = GMSPath(fromEncodedPath: encoded) {
                
                let polyLine = GMSPolyline(path: path)
                polyLine.strokeWidth = 3
                polyLine.map = mapView
                polyLine.zIndex = 203
                polyLine.strokeColor = UIColor.blue
                
                lines.append(polyLine)
                
                let lastPoint = path.coordinate(at: path.count() - 1)
                
                
                if let entrance = route.entrance {
                    let finishingPath = GMSMutablePath()
                    finishingPath.add(lastPoint)
                    finishingPath.add(entrance.coordinates.clCoordinates)
                    
                    let finishingPolyLine = GMSPolyline(path: finishingPath)
                    finishingPolyLine.strokeWidth = 3
                    finishingPolyLine.map = mapView
                    finishingPolyLine.zIndex = 203
                    finishingPolyLine.strokeColor = UIColor.blue.withAlphaComponent(entrance.coordinates.floor == selectedFloor.level ? 1 : 0.3)
                    
                    lines.append(finishingPolyLine)
                }
                
            }
            
        }
        //        for gmLeg in route.gmLegs {
        //
        //            let path =  GMSMutablePath()
        //            path.add(gmLeg.from.clCoordinates)
        //            path.add(gmLeg.to.clCoordinates)
        //
        //            let polyLine = GMSPolyline(path: path)
        //            polyLine.strokeWidth = 3
        //            polyLine.map = mapView
        //            polyLine.zIndex = 203
        //            polyLine.strokeColor = UIColor.blue
        //
        //            lines.append(polyLine)
        //
        //        }
        
    }
    
    
    
    func updateLocationMarker() {
        let iconView = iaLocationMarker.iconView
        if selectedFloor.level == chManager.currentLocation?.floor {
            iconView?.backgroundColor = UIColor.green.withAlphaComponent(1)
            iaLocationMarker.title = nil
        } else {
            iconView?.backgroundColor = UIColor.green.withAlphaComponent(0.3)
            if let level = chManager.currentLocation?.floor {
                iaLocationMarker.title = "Floor №\(level)"
            } else {
                iaLocationMarker.title = nil
            }
        }
        iaLocationMarker.iconView = iconView
        iaLocationMarker.map = nil
        iaLocationMarker.map = self.mapView
    }
    
    
    
    func makeOverlay(with image: UIImage!, plan: Floor!) {
        guard image != nil, plan != nil else { return }
        
        overlays.forEach({ $0.map = nil })
        overlays = []
        
        let pixelToMeterConvertions = 1.0 / ((Double(image.size.width * image.scale) / plan.width + Double(image.size.height * image.scale) / plan.height ) / 2.0)
        let zoomPowered = 156543.03392 * cos(plan.center.lat * .pi / 180) / pixelToMeterConvertions
        
        let logarithmedZoom = log(zoomPowered)/log(2)

        
        let overlay = GMSGroundOverlay(position: plan.center.clCoordinates, icon: image, zoomLevel: CGFloat(logarithmedZoom))
        overlay.bearing = plan.bearing
        overlay.map = mapView
        overlay.zIndex = 100
        overlays.append(overlay)
    }
    
    
    func addRoute(to room: Room, animate: Bool) {
        guard let entrance = room.location else { return }
        
        mode = .route
        directionMarker?.map = nil
        
        routeLevel = selectedFloor.level
        
        chManager.graph?.getRoute(to: entrance.coordinate, floor: MainService.floor(for: room)?.level, completion: { [weak self] (route) in
            guard let route = route else { return }
            self?.redrawCustomRoute(route)
        })
        
        if animate {
            mapView.animate(toLocation: entrance.coordinate)
            mapView.animate(toZoom: 19)
        }
    }
    
    func showOnMap(_ room: Room) {
        
        mode = .normal
        focusedRoom = room
        selectedFloor = MainService.floor(for: room)
        
        if let title = room.title {
            mapView.animate(toLocation: title.coordinate)
        }
        
        mapView.animate(toZoom: 22)
    }
    
    
    
    @IBAction func routeCancelTapped(_ sender: UIButton) {
        chManager.graph?.currentRoute = nil
        lines.forEach({$0.map = nil})
        markers.forEach({$0.map = nil})
        markers.removeAll()
        mode = .normal
        directionMarker?.map = nil
        directionMarker = nil
    }
    
    
    @IBAction func changeFloorTapped(_ sender: UIButton) {
        if let floor = chManager.currentLocation?.floor {
            chManager.lock(floor: floor)
            selectedFloor = MainService.floor(for: floor)
            currentFloor = selectedFloor
            floorView.isHidden = true
        }
        
    }
    
    @IBAction func cancelChangingFloorTapped(_ sender: UIButton) {
        floorView.isHidden = true
        currentFloor = MainService.floor(for: chManager.currentLocation?.floor)
    }
    
    
}

extension MapViewController: FloorSwitcherDelegate {
    
    func floorSwitched(to floor: Floor) {
        selectedFloor = floor
    }
    
}

extension MapViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            manager.startUpdatingLocation()
            manager.startUpdatingHeading()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print(#function, locations)
        
        if let location = locations.first, !wasAnimated {
            wasAnimated = true
            mapView.animate(toLocation: location.coordinate)
            mapView.animate(toZoom: 18)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        compassIcon.transform = CGAffineTransform(rotationAngle: CGFloat(360 - newHeading.trueHeading) * .pi / 180)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(#function, error)
    }
    
}

extension MapViewController: CHLocationManagerDelegate {
    
    func chLocationManager(_ manager: CHLocationManager, didUpdate location: Coordinates) {
        
        floorView?.isHidden = true
        
        if currentFloor == nil {
            currentFloor = MainService.floor(for: location.floor)
        } else if currentFloor?.level != location.floor {
            if let floor = location.floor {
                floorLabel?.text = "Did you move to floor №\(floor)?"
                floorView?.isHidden = false
            }
        }
        
        CATransaction.flush()
        CATransaction.begin()
        CATransaction.setAnimationDuration(3)
        
        iaLocationMarker.position = location.clCoordinates
        updateLocationMarker()
        
        CATransaction.commit()
        
    }
    
    func chLocationManager(_ manager: CHLocationManager, didUpdateSDK sdkLocation: Coordinates) {
        guard chManager.iaManager.delegate != nil else { return }
        iaLocationZone.position = sdkLocation.clCoordinates
    }
    
    func chLocationManager(_ manager: CHLocationManager, didUpdate route: CustomRoute, nodeChanged: Bool) {
        self.redrawCustomRoute(route, nodeChanged: nodeChanged)
    }
    
    func chLocationManager(_ manager: CHLocationManager, didUpdate heading: CLLocationDirection) {
        //        guard mode == .route && routeWasBuilt else { return }
        //        mapView.animate(toBearing: heading)
    }
    
    func chLocationManager(_ manager: CHLocationManager, didUpdate calibrationQuality: ia_calibration) {
        switch calibrationQuality {
        case .iaCalibrationExcellent:
            
            iaLocationMarker.iconView?.backgroundColor = UIColor.green.withAlphaComponent(0.9)
            
        case .iaCalibrationGood:
            
            iaLocationMarker.iconView?.backgroundColor = UIColor.yellow.withAlphaComponent(0.9)
            
        case .iaCalibrationPoor:
            
            iaLocationMarker.iconView?.backgroundColor = UIColor.red.withAlphaComponent(0.9)
        }
    }
    
}


extension MapViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        iaLocationZone.map = position.zoom >= 19 ? mapView : nil
        mapView.isMyLocationEnabled = position.zoom < 19 || overlays.count == 0
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        if let roomLabel = roomLabels.first(where: { $0 == marker }) {
            if let room = selectedFloor.rooms.first(where: { $0.title?.coordinate == roomLabel.position }) {
                if room.number == focusedRoom?.number {
                    let alert = UIAlertController(title: nil, message:
                        String.localizedStringWithFormat(NSLocalizedString("Make a route to", comment: ""), "\(room.title?.text ?? room.number)")
                        , preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default, handler: { (_) in
                        self.addRoute(to: room, animate: true)
                    }))
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    focusedRoom = room
                }
            }
        }
        
        return false
    }
    
    
    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
        chManager.iaManager.delegate = nil
    }
    
    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
        let markerLocation = Coordinates(coordinate: marker.position, floor: selectedFloor.level)
        chManager.handle(new: markerLocation)
    }
    
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        chManager.iaManager.delegate = chManager
    }
    
    func mapView(_ mapView: GMSMapView, didTap overlay: GMSOverlay) {
        guard overlays.contains(where: {$0 == overlay as? GMSGroundOverlay}) else { return }
        
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        print("coordinate = \(coordinate)")
    }
    
}

extension Double {
    
    var distanceString: String {
        get {
            if self < 1000 {
                return String(self.rounded(to: 1)) + " m"
            } else {
                return String((self/1000).rounded(to: 2)) + " km"
            }
        }
    }
}
