//
//  OfficeViewController.swift
//  CHUC_iOS
//
//  Created by Константин on 22/01/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import UIKit


class OfficeViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var ds: OfficesDataSource!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ds = RealDS()
        tableView.tableFooterView = UIView()
    }
    
    @IBAction func tapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
}


extension OfficeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ds.numberOfItems()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = OfficeTableCell.dequeue(from: tableView)
        cell.update(with: ds.item(at: indexPath.row), delegate: self)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        ds.item(at: indexPath.row).isExpended = !(ds.item(at: indexPath.row).isExpended ?? false)
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
}

extension OfficeViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        ds.textChanged(newText: searchText)
        tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}


extension OfficeViewController: OfficeTableCellDelegate {
    
    private func room(for cell: OfficeTableCell) -> Room? {
        guard let row = tableView.indexPath(for: cell)?.row else { return nil }
        guard ds.numberOfItems() > row else { return nil }
        return ds.item(at: row)
    }
    
    private var mapController: MapViewController? {
        get {
            return (self.tabBarController?.viewControllers?.first as? UINavigationController)?.viewControllers.first as? MapViewController
        }
    }
    
    func showOnMap(for cell: OfficeTableCell) {
        guard let room = room(for: cell) else { return }
        
        mapController?.showOnMap(room)
        self.tabBarController?.selectedIndex = 0
    }
    
    func getDirection(for cell: OfficeTableCell) {
        guard let room = room(for: cell) else { return }
        
        mapController?.addRoute(to: room, animate: true)
        self.tabBarController?.selectedIndex = 0

    }
    
    
}
