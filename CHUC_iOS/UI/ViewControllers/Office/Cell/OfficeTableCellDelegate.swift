//
//  OfficeTableCellDelegate.swift
//  CHUC K
//
//  Created by Константин on 06/03/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import Foundation

protocol OfficeTableCellDelegate: class {
    func showOnMap(for cell: OfficeTableCell)
    func getDirection(for cell: OfficeTableCell)
}
