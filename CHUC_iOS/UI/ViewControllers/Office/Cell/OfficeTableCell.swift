//
//  OfficeTableCell.swift
//  CHUC_iOS
//
//  Created by Константин on 22/01/2019.
//  Copyright © 2019 Константин. All rights reserved.
//

import Foundation
import UIKit


class OfficeTableCell: UITableViewCell {
    static let id = "officeCell"
    
    class func dequeue(from tableView: UITableView) -> OfficeTableCell {
        return tableView.dequeueReusableCell(withIdentifier: OfficeTableCell.id) as! OfficeTableCell
    }
    
    
    weak var delegate: OfficeTableCellDelegate?
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var buttonsView: UIView!
    @IBOutlet weak var findButton: UIButton!
    @IBOutlet weak var getButton: UIButton!
    
    
    func update(with office: Room, delegate _delegate: OfficeTableCellDelegate?) {
        delegate = _delegate
        
        title.text = office.number
        subtitle.text = office.title?.text
        buttonsView.isHidden = !(office.isExpended ?? false)
    }
    
    
    @IBAction func findTapped(_ sender: UIButton) {
        delegate?.showOnMap(for: self)
    }
    
    @IBAction func directionTapped(_ sender: UIButton) {
        delegate?.getDirection(for: self)
    }
    
}
